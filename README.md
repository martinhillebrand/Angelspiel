# Angelspiel

Angelspiel ist die Implementierung eines Experimentaldesigns fuer eine Studienreihe zur Untersuchung von Entscheidungen unter Untersicherheit. Entstanden ist es im Rahmen einer HiWi-Taetigkeit am Lehrstuhl für Persönlichkeitspsychologie und Diagnostik der Universität Bamberg bei Dr. Johannes Leder.

## Hinweis

- Eine umfassene Dokumentation inklusive Konfigurationsmoeglichkeiten kann unter [src/Dokumentation_Angelspiel.pdf](https://gitlab.com/martinhillebrand/Angelspiel/blob/master/src/Dokumentation_Angelspiel.pdf) angesehen werden
- Der Code befindet sich in  [src/angelspiel](https://gitlab.com/martinhillebrand/Angelspiel/tree/master/src/angelspiel)



## Vorraussetzungen

Python 2.7 (es werden nur Standard Libraries benutzt)



## Installation

Zip-Datei und entpacken oder (falls git installiert) ```git clone git@gitlab.com:martinhillebrand/Angelspiel.git``` oder ```git clone https://gitlab.com/martinhillebrand/Angelspiel.git```



## Autor

* **Martin Hillebrand** - *Design, Entwicklung, Testung* - [MartinHillebrand](https://gitlab.com/martinhillebrand)

