# -*- coding: utf-8 -*
'''
Created on 03.08.2018

@author: Martin
'''

from Tkinter import  *
from PIL import Image, ImageTk
import time 
import tkFont
import tkMessageBox

#Test für Ergebnistabelle
#plus zusätzlich in Datei speichern


class EndTable_View:
    def __init__(self, view, master,n_lakes ,list_n_fish_caught,list_n_trials):
        
        """ zu verändernder Text hier:"""
        """
            ++++++++++++++++++++++++++++++++++++++
        """
        self.TEXT_HEAD = "Insgesamt geangelte Fische"
        self.TEXT_BUTTON_START="Beenden\n"        
        self.FONT_GENERAL="Arial" 
        self.COLOUR_TEXT = "white"       
        self.COLOUR_GENERAL="black"
        self.TEXT_TEICH = "Teich"
        self.TEXT_INSG = "insg."
        self.COLOUR_HIGHLIGHT = "yellow"
        self.FONTSIZE_HIGHLIGHT_RESIZEFACTOR = 1.3
        
        self.TEXT_QUITREQUESTWINDOW_HEAD =  "Sicher beenden?"
        self.TEXT_QUITREQUESTWINDOW_MAIN =   "Das Programmm soll erst beendet werden, wenn der Versuchsleiter da war.\nWollen Sie das Programm beenden?"
        
        """
        ++++++++++++++++++++++++++++++++++++++
        """
        
        
        
        

        self.view = view

        
        self.master = master
        self.master.attributes('-fullscreen', True)

        ###Texte

        self.width_screen = master.winfo_screenwidth()
        self.height_screen = master.winfo_screenheight()
        self.width_cell_frameMainGrid = self.width_screen*1.0/6        
        self.height_cell_frameMainGrid = self.height_screen*1.0/6

        self.master.configure(background=self.COLOUR_GENERAL)
        
        self.fontsize_label_HEAD_SCALE = (1.0/3) #default (1.0/3)
        self.xc_label_HEAD = int(self.height_cell_frameMainGrid * (1.0/3))
        self.yc_label_HEAD= int((1.0/3)*self.height_cell_frameMainGrid)  
        self.fontsize_label_HEAD = int(self.fontsize_label_HEAD_SCALE*self.height_cell_frameMainGrid)
        self.label_HEAD = Label(master, text=self.TEXT_HEAD,  bg=self.COLOUR_GENERAL, 
                                fg=self.COLOUR_TEXT, font = (self.FONT_GENERAL,self.fontsize_label_HEAD))
        #put it on screen
        self.label_HEAD.place(x=self.xc_label_HEAD,y=self.yc_label_HEAD)
               
                

        
      
        tableframe = Frame(self.master, bg=self.COLOUR_GENERAL) # mit "" wirds transparent!        
        tableframe.place(x=1*self.width_cell_frameMainGrid,
                         y=1*self.height_cell_frameMainGrid,
                         width=4*self.width_cell_frameMainGrid, 
                         height= 3*self.height_cell_frameMainGrid
                         )
        
        totalframe = Frame(self.master, bg=self.COLOUR_GENERAL) # mit "" wirds transparent!        
        totalframe.place(x=1*self.width_cell_frameMainGrid,
                         y=4.5*self.height_cell_frameMainGrid,
                         width=4*self.width_cell_frameMainGrid, 
                         height= 0.5*self.height_cell_frameMainGrid
                         )


        heigth_per_line = int(3*self.height_cell_frameMainGrid *1.0/ n_lakes)
        fontsize_line = int(min(self.height_cell_frameMainGrid/4, heigth_per_line*0.6))
        

        
        for i in range(1,n_lakes+1):
            # Teich
            label_row_lake_frame = Frame(tableframe,bg=self.COLOUR_GENERAL )
            label_row_lake_frame.place(x=0,
                                       y=(i-1)*heigth_per_line,
                                       width = 2*self.width_cell_frameMainGrid,
                                       height = heigth_per_line                                       
                )
            
            label_row_lake = Label(label_row_lake_frame, text=self.TEXT_TEICH+" "+str(i),  bg=self.COLOUR_GENERAL, 
                                fg=self.COLOUR_TEXT, font = (self.FONT_GENERAL,fontsize_line))
            label_row_lake.place(relx=0.5, rely=0.5, anchor=CENTER)
            
            ##gefangene Fische
            label_row_nfc_frame = Frame(tableframe,bg=self.COLOUR_GENERAL )
            label_row_nfc_frame.place(relx=0.5,
                                       y=(i-1)*heigth_per_line,
                                       width = (2.0/3)*self.width_cell_frameMainGrid,
                                       height = heigth_per_line                                       
                )
            
            label_row_nfc = Label(label_row_nfc_frame, text=str(list_n_fish_caught[i-1]),  bg=self.COLOUR_GENERAL, 
                                fg=self.COLOUR_TEXT, font = (self.FONT_GENERAL,fontsize_line))
            label_row_nfc.place(relx=0.5, rely=0.5, anchor=CENTER)
            ## /
            label_row_slash_frame = Frame(tableframe,bg=self.COLOUR_GENERAL )
            label_row_slash_frame.place(relx=2.0/3,
                                       y=(i-1)*heigth_per_line,
                                       width = (2.0/3)*self.width_cell_frameMainGrid,
                                       height = heigth_per_line                                       
                )
            
            label_row_slash = Label(label_row_slash_frame, text="/",  bg=self.COLOUR_GENERAL, 
                                fg=self.COLOUR_TEXT, font = (self.FONT_GENERAL,fontsize_line))
            label_row_slash.place(relx=0.5, rely=0.5, anchor=CENTER)
            
            ## all trials
            label_row_at_frame = Frame(tableframe,bg=self.COLOUR_GENERAL)
            label_row_at_frame.place(relx=5.0/6,
                                       y=(i-1)*heigth_per_line,
                                       width = (2.0/3)*self.width_cell_frameMainGrid,
                                       height = heigth_per_line                                       
                )
            
            label_row_at = Label(label_row_at_frame, text=str(list_n_trials[i-1]),  bg=self.COLOUR_GENERAL, 
                                fg=self.COLOUR_TEXT, font = (self.FONT_GENERAL,fontsize_line))
            label_row_at.place(relx=0.5, rely=0.5, anchor=CENTER)
        
        
        
        
        
        #insg.
        n_fish_caught_total = sum(list_n_fish_caught)
        n_trials_total = sum(list_n_trials)
        
        fontsize_last_line= int(fontsize_line*self.FONTSIZE_HIGHLIGHT_RESIZEFACTOR)
        # insg.
        label_row_insg_frame = Frame(totalframe,bg=self.COLOUR_GENERAL )
        label_row_insg_frame.place(x=0,
                                   y=0,
                                   width = 2*self.width_cell_frameMainGrid,
                                   height = 0.5*self.height_cell_frameMainGrid                                      
            )
        
        label_row_insg = Label(label_row_insg_frame, text=self.TEXT_INSG ,  bg=self.COLOUR_GENERAL, 
                            fg=self.COLOUR_HIGHLIGHT, font = (self.FONT_GENERAL,fontsize_last_line))
        label_row_insg.place(relx=0.5, rely=0.5, anchor=CENTER)
        
        ##gefangene Fische
        label_row_nfc_frame = Frame(totalframe,bg=self.COLOUR_GENERAL )
        label_row_nfc_frame.place(relx=0.5,
                                     y=0,
                                   width = (2.0/3)*self.width_cell_frameMainGrid,
                                   height = 0.5*self.height_cell_frameMainGrid                                     
            )
        
        label_row_nfc = Label(label_row_nfc_frame, text=str(n_fish_caught_total),  bg=self.COLOUR_GENERAL, 
                            fg=self.COLOUR_HIGHLIGHT, font = (self.FONT_GENERAL,fontsize_last_line))
        label_row_nfc.place(relx=0.5, rely=0.5, anchor=CENTER)
        ## /
        label_row_slash_frame = Frame(totalframe,bg=self.COLOUR_GENERAL )
        label_row_slash_frame.place(relx=2.0/3,
                                     y=0,
                                   width = (2.0/3)*self.width_cell_frameMainGrid,
                                   height = 0.5*self.height_cell_frameMainGrid                                    
            )
        
        label_row_slash = Label(label_row_slash_frame, text="/",  bg=self.COLOUR_GENERAL, 
                            fg=self.COLOUR_HIGHLIGHT, font = (self.FONT_GENERAL,fontsize_last_line))
        label_row_slash.place(relx=0.5, rely=0.5, anchor=CENTER)
        
        ## all trials
        label_row_at_frame = Frame(totalframe,bg=self.COLOUR_GENERAL)
        label_row_at_frame.place(relx=5.0/6,
                                     y=0,
                                   width = (2.0/3)*self.width_cell_frameMainGrid,
                                   height = 0.5*self.height_cell_frameMainGrid                                      
            )
        
        label_row_at = Label(label_row_at_frame, text=str(n_trials_total),  bg=self.COLOUR_GENERAL, 
                            fg=self.COLOUR_HIGHLIGHT, font = (self.FONT_GENERAL,fontsize_last_line))
        label_row_at.place(relx=0.5, rely=0.5, anchor=CENTER)
        
        ##### ok button
        fontsize_button = self.height_screen/30
        self.button_start = Button(self.master, 
                                   text=self.TEXT_BUTTON_START, 
                                   command=self.nachfragen_schliessen,
                                   font=(self.FONT_GENERAL, fontsize_button),bg=self.COLOUR_GENERAL, fg=self.COLOUR_TEXT,)
        self.button_start.place(relx=0.9, rely=0.1, anchor=CENTER)



        
    def nachfragen_schliessen(self):
        antwort = tkMessageBox.askquestion(self.TEXT_QUITREQUESTWINDOW_HEAD, 
                                 self.TEXT_QUITREQUESTWINDOW_MAIN, 
                                 default=tkMessageBox.NO                                
                                 )
        if antwort == "yes":            
            self.view.controler.view_closeAll()
