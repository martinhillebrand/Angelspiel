'''
Created on 03.07.2017

@author: Martin
'''

import datetime


class WriterReader(object):
    '''
    classdocs
    '''

    def __init__(self, model):
        '''
        Constructor
        '''
                
        self.model = model
        
        self.PATH_VPNCOUNTER = "VPN_Counter.txt"
        self.PATH_SAVINGFOLDER = "data/"               
        
        self.PATH_SESSIONOVERVIEW = "input_data/_overview.txt"
        self.PATH_BEGINNING_SESSION = "input_data/game_"
        self.PATH_IMG ="img/"
        
        self.date = None # Datum, an dem das Spiel gespielt wurde
        self.n_Vpn = None #as string!
        self.read_n_Vpn()
        
        self.filename_results = None
        self.initialise_Filename()
        
        self.path_file =  self.PATH_SAVINGFOLDER+self.filename_results
        self.path_file_confidence = self.path_file + "_confidence"
        self.path_file_summary = self.path_file + "_summary"
        #create new outputfiles        
        self.datafile_results = open(self.path_file, "a")
        self.datafile_confidence = open(self.path_file_confidence, "a")
        self.datafile_summary = open(self.path_file_summary, "a")
        
        #self.write_basicInfo()


    def read_n_Vpn(self): # read next VPN - NUmmer and increase it
        datafile = open(self.PATH_VPNCOUNTER, "r")
        vpnCounter = datafile.readline()
        self.n_Vpn = vpnCounter # as string!
        self.model.n_subject = int(self.n_Vpn)
        datafile.close()
        
        vpnCounter_int = int(vpnCounter)+1
        vpnCounter_neu = str(vpnCounter_int)
        
        datafile = open("VPN_Counter.txt", "w")
        datafile.write(vpnCounter_neu)
        datafile.close()
     
    def initialise_Filename(self):
        n = self.n_Vpn
        len_n = len(n)
        for i in range(len_n,6):
            n = "0"+n
            
        now  = datetime.datetime.now()        
        self.date = now.strftime("%Y-%m-%d_%H-%M")
         
        self.filename_results = n+"_Angelspiel_"+ self.date
    
    def write_basicInfo(self):
        line = "subject_nr, subject_age, subject_sex, test_date\n" +  (self.filename_results.split("_"))[0] +  ","+str(self.model.age_subject)+","+str(self.model.sex_subject)+","+self.date +"\n"
        self.datafile_results.write(line)
        self.datafile_results.write("---------------------------------------------\n")
        self.datafile_results.write("chronological_session_nr,session_nr,directFeedback_type,summaryFeedback_type,nr_baits,trial_type,trial_number,trial_number_exploit,p_1,p_2,p_3,p_4,p_5,p_6,fish_1,fish_2,fish_3,fish_4,fish_5,fish_6,reaction_time,chosen_alternative,caught_fish\n")        
    
    def write_basicInfo_ConfidenceFile(self):
        line = "subject_nr, subject_age, subject_sex, test_date\n" +  (self.filename_results.split("_"))[0] +  ","+str(self.model.age_subject)+","+str(self.model.sex_subject)+","+self.date +"\n"
        self.datafile_confidence.write(line)
        self.datafile_confidence.write("---------------------------------------------\n")
        self.datafile_confidence.write("chronological_session_nr,session_nr, directFeedback_type,summaryFeedback_type,nr_baits,estimated_caught_nfish, estimated_confidence,time_needed\n")        
        
    
    def write_trialInfo(self, chronological_session_nr,session_nr,directFeedback_type,summaryFeedback_type,nr_baits,trial_type,trial_number,trial_number_exploit,p_1,p_2,p_3,p_4,p_5,p_6,fish_1,fish_2,fish_3,fish_4,fish_5,fish_6,reaction_time,chosen_alternative,caught_fish         #specific for each trial 
                        ):
        

        lineElements = [chronological_session_nr,session_nr,directFeedback_type,summaryFeedback_type,nr_baits,trial_type,trial_number,trial_number_exploit-1,p_1,p_2,p_3,p_4,p_5,p_6,fish_1,fish_2,fish_3,fish_4,fish_5,fish_6,reaction_time,chosen_alternative,caught_fish]

        line = ""
        for i in range(0,len(lineElements)-1):
            line += str(lineElements[i])+","
    
        line+=(str(lineElements[len(lineElements)-1]) +"\n")
        self.datafile_results.write(line)   
    
    def write_confidenceData(self,chronological_session_nr, session_nr, directFeedback_type,summaryFeedback_type,nr_baits,estimated_caught_nfish, estimated_confidence,time_needed):
        line = ",".join(map(str, [chronological_session_nr,session_nr, directFeedback_type,summaryFeedback_type,nr_baits,estimated_caught_nfish, estimated_confidence,time_needed]))
        line +="\n"
        self.datafile_confidence.write(line)
    
    def write_summaryData(self,n_lakes, list_n_fish_caught, list_n_trials  ):
        self.datafile_summary.write("insgesamt gefangen\n")
        for i in range(0,n_lakes):
            line = "\t".join(["Teich "+str(i),str(list_n_fish_caught[i]),"/",str(list_n_trials[i])+"\n"])
            self.datafile_summary.write(line)
        self.datafile_summary.write("\n\nInsgesamt\t"+str(sum(list_n_fish_caught))+"\t/\t"+str(sum(list_n_trials)))
        self.datafile_summary.close()            
        
    def read_list_ref_sessions(self):
        file_overviewsession = open(self.PATH_SESSIONOVERVIEW, "r")        
        list_overviewsessions = file_overviewsession.read().split("\n")
        file_overviewsession.close()
     
        overviewsession =  list_overviewsessions[int(self.n_Vpn)%len(list_overviewsessions)]  # modulo deshalb, weil fasll mehr vpn nummern als session -> beginn von vorne!
         
        overviewsession = overviewsession.split(",")
         
        self.model.list_ref_sessions = overviewsession
        self.model.n_totalSessions = len(overviewsession)
        self.model.list_n_fish_caught = [0 for x in range(0,self.model.n_totalSessions )]
        self.model.list_n_trials = [0 for x in range(0,self.model.n_totalSessions )]

        
                
    def read_list_sessions(self):
        for reference in self.model.list_ref_sessions:
            self.read_session(reference)        
        
    
    def read_session(self,reference ):
        
        file_session = open(self.PATH_BEGINNING_SESSION+reference+".txt", "r")
        text_session = file_session.read().split("\n")
        
        generalinfo = text_session.pop(0).split(",")
        
        
        [directFeedback_type, 
         summaryFeedback_type, 
         path_img_lake, 
         n_baits] = generalinfo[0:4]
             
        path_img_lake = self.PATH_IMG+path_img_lake
        n_baits = int(n_baits)
         
        
        paths_img_bait = [None for x in range(0,n_baits)]
        paths_img_bait_fishfull = [None for x in range(0,n_baits)]
        paths_img_bait_fishempty = [None for x in range(0,n_baits)]
         
        for i_bait in range(0,n_baits):
            cur_index = 4+i_bait*3
            paths_img_bait[i_bait] = generalinfo[cur_index]
            paths_img_bait_fishfull[i_bait] = generalinfo[cur_index+1]
            paths_img_bait_fishempty[i_bait] = generalinfo[cur_index+2]
                
        
        paths_img_bait = [self.PATH_IMG+x for x in paths_img_bait]
        paths_img_bait_fishfull = [self.PATH_IMG+x for x in paths_img_bait_fishfull]
        paths_img_bait_fishempty = [self.PATH_IMG+x for x in paths_img_bait_fishempty]
                
        list_sections =[]
        for line in text_session:      
            
            curr_line =line.split(",")
            length_section = int(curr_line[0])
            p_vaues = [-1 for x in range(0,n_baits)]
            
            for i in range(0,n_baits):
                p_vaues[i] = float(curr_line[i+1])
            section = [length_section]+ p_vaues
       
            list_sections.append(section)
        
        
        
        self.model.addSession(
            directFeedback_type, summaryFeedback_type, list_sections, 
                 path_img_lake, 
                 paths_img_bait,
                 paths_img_bait_fishfull,
                 paths_img_bait_fishempty,
                 n_baits
          
            )       
        
           
        
        
    
    def closeAndSave_FileResults(self):
        self.datafile_results.close()
        self.datafile_confidence.close()  
        
        
