# -*- coding: utf-8 -*
'''
Created on 02.07.2017

@author: Martin
'''
from Tkinter import  *
from PIL import Image, ImageTk








#takes a picture and dimensions of a a rectangle and rescales the pictures so that it fits in optimally
def rescalePicture(image,  label_width, label_height):
    img_width = float(image.size[0])
    img_height = float(image.size[1])
    (new_width, new_height) = rescalePicture_Pixels(img_width, img_height , label_width, label_height)
    new_width = int(new_width)
    new_height = int(new_height)
    
    image = image.resize((new_width, new_height), Image.ANTIALIAS)    
    return image    


def rescalePicture_Pixels(img_width, img_height , label_width, label_height):

    wpercent = (label_width*1.0 / img_width)
    hpercent = (label_height*1.0 / img_height)
    
    #determine what needs to be resized!
    if wpercent<1 and hpercent <1: #Bild in beide Dimensionen zu gross -> verkleinern
        if wpercent>hpercent: # wo die diskrepanz groesser (d.h. wert kleiner ist, in diese richtung msus verkleintert werden!
            return (hpercent*img_width,label_height)            
        else:
            return (label_width, wpercent*img_height)
    elif hpercent <1 : # nur bild zu hoch!
        return (hpercent*img_width,label_height)
    elif wpercent<1 : #nur bild zu brei
        return (label_width,wpercent*img_height)
    else: #bild in beide dimensionen zu klein, => das mit der kleineren disrepanz (kleine zahl) groesser machen
        if wpercent<hpercent: 
            return (label_width,wpercent*img_height)
        else:
            return (hpercent*img_width,label_height)