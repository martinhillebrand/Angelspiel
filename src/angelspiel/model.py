# coding: utf8
'''
Created on 27.06.2017

@author: Martin
'''
# coding: utf8
import controler
import model_writerReader
from random import shuffle
### IMPORTANT IMPORTANT IMPORTANT

class Model(object):
    
    def __init__(self, controler):
        # hier kommt das datenmodell
        self.age_subject = None
        self.sex_subject = None
        self.n_subject = None
        
        self.list_ref_sessions = [] #list with references to sessions
        self.list_sessions = [] # list with actual sessions
        self.n_currentSession = 0
        self.n_totalSessions = None # length of sessionlist
        self.currentSession = None
        
        
        self.controler = controler
        self.writerReader = model_writerReader.WriterReader(self) 
        
        
        # für Feedback Screen am Ende
        self.list_n_fish_caught = []
        self.list_n_trials = []
       
        
    def setAgeSex(self, age, sex):
        self.age_subject = age
        self.sex_subject = sex
        self.initialise_model() # wichtig dazu: n_currentSession, 
        self.writerReader.write_basicInfo() # wichtig dazu age und sex!
        self.writerReader.write_basicInfo_ConfidenceFile()        
        
    
    def instructionAccepted(self):
        self.currentSession = self.list_sessions[self.n_currentSession]
        self.transmit_sessionData()
    
    def endSession(self):
        self.n_currentSession  += 1
        if(self.n_currentSession <self.n_totalSessions): #mind eine Runde geht noch!
            self.currentSession = self.list_sessions[self.n_currentSession]
            self.transmit_sessionData() # controler ruft model auf!
        else: #das war schon die letzte runde
            self.writerReader.closeAndSave_FileResults()
            self.controler.model_showEndview()
        
    
    def transmit_sessionData(self):       

        self.controler.model_startSession(
            self.currentSession.variant_session,
            self.currentSession.directFeedback_type, self.currentSession.summaryFeedback_type, self.currentSession.list_trials_explore, self.currentSession.list_trials_exploit,
            self.currentSession.path_img_lake, 
            
            self.currentSession.n_baits,
            self.currentSession.paths_img_baits, #list of paths to baits
            self.currentSession.paths_img_feedback_fishfull,
            self.currentSession.paths_img_feedback_fishempty
            )
    
    def add_trialData(self, n_currentTrial, button, reactiontime, explore_exploit ):
               
        session_nr = self.list_ref_sessions[self.n_currentSession] #bezieht sich nicht auf reihenfolge, sondern auf relativen bezug
        directFeedback_type = self.currentSession.directFeedback_type
        summaryFeedback_type = self.currentSession.summaryFeedback_type
        
        trial_type  = explore_exploit
        trial_number = self.currentSession.n_currentTrial_Explore + self.currentSession.n_currentTrial_Exploit
        
        fishes = [] 
        
        
        reaction_time = reactiontime 
        chosen_alternative = button
        
        p_values = [] 

              

        
        fish_caught =0
        
        if explore_exploit == 0: #explore, dazu muss self.n_trial_explore>0 sein -> modulo geht  
            p_values= self.currentSession.p_values_explore 
            p_values = list(p_values)+ [-1]*(6-len(p_values)) # mit -1 auffüllen            
            
            fishes =self.currentSession.list_trials_explore[self.currentSession.n_currentTrial_Explore % self.currentSession.n_trial_explore ]
            fishes = list(fishes)
            fishes = fishes+ [-1]*(6-len(fishes)) # mit -1 auffüllen   
            
            self.currentSession.n_currentTrial_Explore +=1

            
            if fishes[chosen_alternative]==1:
                fish_caught = 1 
            
        else: #exploit            
            
            p_values = self.currentSession.list_of_probabilities_exploit[self.currentSession.n_currentTrial_Exploit]
            p_values = list(p_values)+ [-1]*(6-len(p_values)) # mit -1 auffüllen
            
            
            fishes =self.currentSession.list_trials_exploit[self.currentSession.n_currentTrial_Exploit]
            fishes = list(fishes)
            fishes = fishes+ [-1]*(6-len(fishes))           

                       
            self.currentSession.n_currentTrial_Exploit +=1
            
            ## updaten der zähler für summary am ende
            self.list_n_trials[self.n_currentSession] += 1
            # fisch gefangen (button: 0 links; 1 rechts
            
            
            if fishes[chosen_alternative]==1:
                fish_caught = 1 
                
            
            self.list_n_fish_caught[self.n_currentSession] += fish_caught
        
        
        
        self.writerReader.write_trialInfo(*([self.n_currentSession,
                                          session_nr,  
                                          directFeedback_type, 
                                          summaryFeedback_type, 
                                          self.currentSession.n_baits,    #equal for entire session
                        
                                          trial_type,trial_number, self.currentSession.n_currentTrial_Exploit]+                                   
                                           p_values+
                                            fishes+
                                          [reaction_time, chosen_alternative, fish_caught])       #specific for each trial 
                                            )

        
    def add_confidenceData(self,estimated_caught_nfish, estimated_confidence,time_needed):
        session_nr = self.list_ref_sessions[self.n_currentSession] #bezieht sich nicht auf reihenfolge, sondern auf relativen bezug
        directFeedback_type = self.currentSession.directFeedback_type
        summaryFeedback_type = self.currentSession.summaryFeedback_type
        self.writerReader.write_confidenceData(self.n_currentSession,session_nr,directFeedback_type,summaryFeedback_type,self.currentSession.n_baits ,estimated_caught_nfish, estimated_confidence,time_needed)
    
    """
chronological_session_nr, session_nr, directFeedback_type,summaryFeedback_type,nr_baits,estimated_caught_nfish, estimated_confidence,time_needed
"""
    
    def initialise_model(self):
        #TODO
        # hier alle restlcihen attribute definieren:
        """
        self.list_ref_sessions = [] X
        self.list_sessions = []
        self.n_currentSession = 0 
        self.n_totalSessions = None # length of list_sessions X
        self.currentSession = None
        """
        
        #um list sessionszu füllen (liste mit objekten vom typ session
        # braucht man erst eine liste mit referenzen (sessionnummbern) anhand der vpnNummer
        self.writerReader.read_list_ref_sessions()
        
        self.writerReader.read_list_sessions()
        
        #daraus dann liste mit sessions bekommen
        #daraus dann jeweils triallist einlesen (sowie andere attribute der sessions       
        
    
    def addSession(self,
            directFeedback_type, summaryFeedback_type,  list_sections, 
                 path_img_lake, 
                 paths_img_bait,
                 paths_img_bait_fishfull,
                 paths_img_bait_fishempty   ,
                 n_baits       
            ):
       
        new_session = Session(directFeedback_type, summaryFeedback_type, list_sections, 
                 path_img_lake,  
                 paths_img_bait,
                 paths_img_bait_fishfull,
                 paths_img_bait_fishempty     ,
                 n_baits      
            )
        self.list_sessions.append(new_session)
     
    def showScore(self):
        self.writerReader.write_summaryData(             n_lakes = self.n_totalSessions,
            list_n_fish_caught = self.list_n_fish_caught,                 
            list_n_trials = self.list_n_trials)
        
        self.controler.model_showScore(
            n_lakes = self.n_totalSessions,
            list_n_fish_caught = self.list_n_fish_caught,                 
            list_n_trials = self.list_n_trials
            )
        
        
class Session():
    def __init__(self, directFeedback_type, summaryFeedback_type, list_sections,
                 path_img_lake, 
                 paths_img_baits,
                 paths_img_feedback_fishfull,
                 paths_img_feedback_fishempty,
                 n_baits
                 ):
              
        ##kreieren der Trial-Listen
        self.n_baits = n_baits
        
        
        #Explore
                
        
        explore_section = list_sections[0]
        list_sections.pop(0)        

        self.n_trial_explore = explore_section[0]
        self.p_values_explore = explore_section[1:(1+n_baits)]       

        self.variant_session = self.n_trial_explore

        
        if self.n_trial_explore>0:
            self.variant_session = 1
            ###Three Variants of the Experiment
            ### variant_session == -1 : continue to Exploit Button (=as many exploring as wanted)
            ### variant_session == 0: no explore at all -> immediate exploit
            ### variant_session == 1: fixes number of explore trials -> then exploit
        if self.n_trial_explore==-1:
            self.n_trial_explore=50
        
        
        ##get trial lists EXPLORE
        self.n_currentTrial_Explore = 0
        self.list_trials_explore = self.get_shuffled_trialLists(self.n_trial_explore,                                                                
                                                                self.p_values_explore,
                                                                n_baits)      

        
        ##get trial lists EXPLOIT
        self.n_currentTrial_Exploit=0
        self.list_trials_exploit = []
        self.list_of_probabilities_exploit = []
        for section in list_sections:
            self.list_trials_exploit += self.get_shuffled_trialLists(section[0], section[1:(1+n_baits)],n_baits)
            self.list_of_probabilities_exploit += [tuple(section[1:(1+n_baits)])]*section[0]

        
        self.n_trial_exploit = len(self.list_trials_exploit)
        
        ##feedbacktypes
        self.directFeedback_type = directFeedback_type #0 nur gewählter bait, 1= beide baits
        self.summaryFeedback_type = summaryFeedback_type #0 nur anzahl, 1= sequenz
        ##images
        
        
        self.path_img_lake= path_img_lake 
        
        self.paths_img_baits = paths_img_baits, #list of paths to baits
        self.paths_img_feedback_fishfull = paths_img_feedback_fishfull
        self.paths_img_feedback_fishempty = paths_img_feedback_fishempty
        
 

        
        
        
    def get_shuffled_trialLists(self, n_trials,p_values,n_baits):
        shuffled_trialLists = []
        
        for i_bait in range(0,n_baits):
            n_fishes_bait = int(round(n_trials*p_values[i_bait]))
            fishes_bait = ([1]*n_fishes_bait) + ([0]*(n_trials-n_fishes_bait))
            shuffle(fishes_bait)
            shuffled_trialLists.append(fishes_bait)
        
        
        shuffled_trialLists = zip(*shuffled_trialLists)                               
        
        return shuffled_trialLists 






