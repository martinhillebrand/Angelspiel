# -*- coding: utf-8 -*
'''
Created on 03.07.2017

@author: Martin
'''
from Tkinter import  *
from PIL import Image, ImageTk
import time
import view_helpermethods
import tkFont
import tkMessageBox

class VpnDataView(object):
    
    def __init__(self, view, master):
        """ zu verändernder Text hier:"""
        """
        ++++++++++++++++++++++++++++++++++++++
        """
        
        
        self.PATH_ICON = "img/icon_fishFull.ico"
        self.TEXT_WINDOWTITLE ="Angelspiel V. 2.0"
        
        self.TEXT_LABEL_HEADING = "Bitte Daten eingeben"
        self.TEXT_LABEL_AGE = "Alter (in Jahren)"
        self.TEXT_LABEL_SEX = "Geschlecht (m/w/k)"
        self.TEXT_BUTTON_START="Angelspiel starten"
        self.FONT_GENERAL="Arial"   
        
        self.WARNING_TITLE =     "Eingabe ungültig"
        self.WARNING_CONTENT = "Bitte Alter in Jahren angeben (ganze Zahl) und Geschlecht mit m, w oder k (keine Angabe) eingeben"
        
        """
        ++++++++++++++++++++++++++++++++++++++
        """
        self.view = view
        
        self.age = 0
        self.sex = 0 #0= m, 1= w
        self.master = master
        
        
        #change titlebar
        self.master.iconbitmap(default=self.PATH_ICON)
        self.master.wm_title(self.TEXT_WINDOWTITLE)
        #not resizable
        self.master.resizable(False, False)
       
        #centre of the screen and 1/16 of the screen is covered
        
        self.width_screen = self.master.winfo_screenwidth()        
        self.height_screen = self.master.winfo_screenheight()
        self.width_VpnDataView = self.width_screen/4
        self.height_VpnDataView = self.height_screen/4
        xc_VpnDataView = 3*self.width_screen/8
        yc_VpnDataView = 3*self.height_screen/8       
                #                
        self.master.geometry(
                #str(self.width_VpnDataView)+"x"+str(self.height_VpnDataView)
                             "+"+str(xc_VpnDataView)+"+"+str(yc_VpnDataView))
        
        #define elements of the window
        fontsize_heading =  self.height_VpnDataView/15
        fontsize_label = self.height_VpnDataView/20
        fontsize_button = self.height_VpnDataView/15
        
        self.label_heading = Label(self.master, text=self.TEXT_LABEL_HEADING, font=(self.FONT_GENERAL, fontsize_heading))        
        self.label_age = Label(self.master, text=self.TEXT_LABEL_AGE, font=(self.FONT_GENERAL, fontsize_label))
        self.label_sex = Label(self.master,text= self.TEXT_LABEL_SEX, font=(self.FONT_GENERAL, fontsize_label))        
        self.entry_age = Entry(self.master, font=(self.FONT_GENERAL, fontsize_label))
        self.entry_sex = Entry(self.master, font=(self.FONT_GENERAL, fontsize_label))        
        self.button_start = Button(self.master, text=self.TEXT_BUTTON_START, command=self.start, font=(self.FONT_GENERAL, fontsize_button))
        
        #im fenster platzieren
        px=self.height_screen/200
        py=self.height_screen/200        
        self.label_heading.grid(row=0, column=0, columnspan=2, padx=2*px, pady= 2*py,sticky=W)
        self.label_age.grid(row=1,column=0,padx=px, pady= py, sticky=E)
        self.label_sex.grid(row=2,column=0,padx=px, pady= py,sticky=E)
        self.entry_age.grid(row=1,column=1,padx=px, pady= py,sticky=W)
        self.entry_sex.grid(row=2,column=1,padx=px, pady= py,sticky=E)
        self.button_start.grid(row=3, column=0, columnspan=2,padx=2*px, pady= 2*py,sticky=E)
         
                    
    def start(self):
        self.age = self.entry_age.get()
        self.sex =self.entry_sex.get()
        if not(self.age.isdigit()) or not(self.sex=="m" or self.sex=="w" or self.sex=="k"  ):
            tkMessageBox.showwarning(self.WARNING_TITLE, self.WARNING_CONTENT)
            return
        self.view.controler.view_vpnData_finished(self.age, self.sex) # nächstes fenster ruft controler auf
        
