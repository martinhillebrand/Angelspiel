# -*- coding: utf-8 -*
'''
Created on 30.06.2017

@author: Martin
'''


from Tkinter import  *
from PIL import Image, ImageTk
import time
import view_helpermethods


import tkFont


class ExperimentalView(object):
    '''
    classdocs    '''


    def __init__(self, view, 
                 master, 
                 variant_session,
                 type_feedbackTrial, 
                 type_feedbackOverall, 
                 fishList, 
                 fishList_Exploit,
                 path_img_lake,
                 n_baits,
                 paths_img_baits, #list of paths to baits
                 paths_img_feedback_fishfull,
                 paths_img_feedback_fishempty

                 ):
        

        # master is the main window
        #type_feedbackTrial: 0 iff only for chosen option, 1 iff complete
        #type_feedbackOverall: 0 iff sum, 1 iff sequence
        
        """++++++++++++++++++++++++++++++"""        
        #to be set manually
        self.COLOUR_GENERAL="black"
        self.COLOUR_TEXT = "white"
        self.COLOUR_FRAME_FEEDBACKTRIAL ="orange"
        self.FONT_GENERAL="Arial"
        
        
        self.TEXT_LABEL_ExploreSessionInfo = "Du befindest dich nun an einem neuen Teich\nund hast jetzt Zeit den Teich zu erkunden.\nEs ist egal, wie viele Fische du dabei fängst."
        self.TEXT_BUTTON_CONTINUETOEXPLORE ="Verstanden, los geht's"  
        self.fontsize_intstuctiontext_SCALE = 40 #default 40
        self.fontsize_button_instructiontext_SCALE = 60 #default 60
        self.PATH_IMG_FISHFULL = "img/fish_full.png"
        self.PATH_IMG_FISHEMPTY = "img/fish_empty.png"
        
        self.TEXT_EXPLOREHEADING="Erkundungsphase"
        self.fontsize_label_exploreHeading_SCALE = (1.0/3) #default (1.0/3)
        self.TEXT_BUTTON_CONTINUEWITHEXPLOIT="weiter zur\nWettbewerbsphase"
        self.fontsize_button_continueWithExploit_SCALE =  (1.0/12)  #default (1.0/8)
        
        self.TEXT_FEEDBACKOVERALLHEADING= "Bisheriger Fang" 
        self.fontsize_label_feedbackOverallHeading_SCALE =(1.0/8) #default (1.0/8)        
        self.DURATION_FEEDBACKTRIAL = 500 # Millisekunden, in denen das between-trial-Feedback variieirt wird
        self.COLOURLIST_BACKGROUND_OVERALLFEEDBACK = ["gray1", "RoyalBlue4",  "gray15", "RoyalBlue3","gray30","RoyalBlue2", "gray45", "RoyalBlue1" ] #wurde irrelevant
        self.NUNITSPERROW_FRAME_FEEDBACKOVERALLCONTAINER= 40 # Anzahl an FeedbackUnits, die nebeneinander angezeigt werden.
        self.NROWS_FRAME_FEEDBACKOVERALLCONTAINER= 2 # Anzahl an der Reihen der FeedbackUnits, die überienander in Feedbackfläche angezeigt werden
        self.BOOLEAN_POSITIVE_FEEDBACK_ONLY = True #entscheidet, ob im Gesamtfeedback auch Fehlversuche (leere Fische mit entsprechendem Haken) erscheinen.

        self.TEXT_LABEL_ExploitSessionInfo = "Du kannst in der nun folgenden Wettbewerbsphase 80-mal die Angel auswerfen."
        self.fontsize_instruction_ExploitSessionInfo_text_SCALE = 40 #default 40
        self.TEXT_LABEL_ExploitSessionInfo_Quest_NFishes = "Was glaubst Du, wie viele Fische du bei diesen\nnächsten 80 Versuchen in diesem Teich fängst?"
        self.SCALE_ESTIMATION_UPPERLIMIT = 80 # obergrenze der skala
        self.TEXT_LABEL_ExploitSessionInfo_Quest_Confidence = "Wie sicher bist Du Dir, dass du so viele Fische fängst?"
        self.likertskala_conficence = [ ("absolut\nunsicher",1),  ("sehr\nunsicher",2), ("eher\nunsicher",3), ("weder\nnoch",4), ("eher\nsicher",5), ("sehr\nsicher",6), ("absolut\nsicher",7)]
        self.TEXT_BUTTON_CONTINUETOEXPLOIT ="Weiter zur Wettbewerbsphase"
        self.fontsize_button_ExploitSessionInfo_SCALE = 60 #default 60        
        
        self.TEXT_EXPLOITHEADING="Wettbewerbsphase"  
        
        self.TEXT_LABEL_AFTERSESSION = "Die Wettbewerbsphase an diesem Teich ist vorbei.\nGehe nun weiter zum nächsten Teich"
        self.TEXT_BUTTON_AFTERSESSION = "Weiter zum nächsten Teich"    
  
        


        

        
        """++++++++++++++++++++++++++++++"""
        
        self.variant_session = variant_session
        ##-1 as long exploring as wanted
        ## 0 no exploration
        ## 1 fix number of exploration
        
        
        self.view = view
        

        
        self.in_explore_mode = True
        
        self.FISHLIST = fishList # of kind [(1,1,..), (0,1,..),....]

        self.n_currentTrial = 0
        self.n_maximumTrial = len(self.FISHLIST)
        self.i_currentColour_feedbackOverall =0    
        self.FISHLIST_EXPLOIT = fishList_Exploit        

        
        self.CURRENTNUMBER_OF_EXPLOIT_TRIALS = 0
        self.NUMBER_OF_EXPLOIT_TRIALS = len(self.FISHLIST_EXPLOIT)
        
        
        #Selbsteinschätzung
        self.estimated_caught_nfish = -1
        self.estimated_confidence = 0
        
        
        #FRAME
        self.master = master
        self.master.attributes('-fullscreen', True)
        self.master.configure(background=self.COLOUR_GENERAL)
        

        
        
        
        #General Attributes for the Kind of Game being played, are instantiated immediately     
        ##from constructor
        self.type_feedbackTrial=int(type_feedbackTrial)
        self.type_feedbackOverall=int(type_feedbackOverall)
        
        
        self.n_baits =n_baits
        self.paths_img_baits =paths_img_baits[0] # bufix, paths kommt als liste innerhalb von runden klammenr
        self.paths_img_feedback_fishfull = paths_img_feedback_fishfull
        self.paths_img_feedback_fishempty = paths_img_feedback_fishempty
        
        
        self.path_img_lake = path_img_lake

        

        
        ## to be taken from current environement
        self.width_screen = master.winfo_screenwidth()
        self.height_screen = master.winfo_screenheight()
        
        
        
        #master frame
        self.frame = Frame(self.master, background=self.COLOUR_GENERAL)
        self.frame.place(x=0, y=0, width=self.width_screen, height=self.height_screen)
        

        #images
        self.img_lake= Image.open(self.path_img_lake)
        self.img_fishFull= Image.open(self.PATH_IMG_FISHFULL)
        self.img_fishEmpty = Image.open(self.PATH_IMG_FISHEMPTY) 
        
        self.imgs_baits = [None for x in range(0,self.n_baits)]
        self.imgs_feedback_bait_FishFull =[None for x in range(0,self.n_baits)]
        self.imgs_feedback_bait_FishEmpty =[None for x in range(0,self.n_baits)]
        
        
        
        for i in range(0,self.n_baits):
            self.imgs_baits[i] = Image.open(self.paths_img_baits[i])
            self.imgs_feedback_bait_FishFull[i] = Image.open(self.paths_img_feedback_fishfull[i])
            self.imgs_feedback_bait_FishEmpty[i] = Image.open(self.paths_img_feedback_fishempty[i])
        
        
        #koordinaten und dimensionen der rechtecke: der bildschirm wird in ein 6*6 Gitter eingeteilt -> Breite der Arrays als GLeitkommazahl benötigt!
        self.width_cell_frameMainGrid = self.width_screen*1.0/6 
        self.height_cell_frameMainGrid  = self.height_screen*1.0/6      
        # 18er Frames auch noch
        self.width_cell_frame18Grid = self.width_screen*1.0/18 
        self.height_cell_frame18Grid  = self.height_screen*1.0/18 

        """
        ##KEINE FRAMES!!! ein hauptframe!, nur so kann man überlagernd malen!!!
        """
        #the coordinates of widgets are relative with regards to the full screen!
        ##Widgets  of the top 
        ### exploreHeading
        self.xc_label_exploreHeading = int(self.height_cell_frameMainGrid * (1.0/3))
        self.yc_label_exploreHeading = int((1.0/3)*self.height_cell_frameMainGrid)  
        self.fontsize_label_exploreHeading  = int(self.fontsize_label_exploreHeading_SCALE*self.height_cell_frameMainGrid)
        self.label_mainHeading = Label(self.frame, text=self.TEXT_EXPLOREHEADING,  bg=self.COLOUR_GENERAL, fg=self.COLOUR_TEXT, font = (self.FONT_GENERAL,self.fontsize_label_exploreHeading))
        self.label_mainHeading.place(x=self.xc_label_exploreHeading,y=self.yc_label_exploreHeading)
        

        ### BUTTON_continueWithExploit
        self.xc_button_continueWithExploit = int(5*self.width_cell_frameMainGrid+(1.0/4)*self.height_cell_frameMainGrid)
        self.yc_button_continueWithExploit = int((1.0/4)*self.height_cell_frameMainGrid)
        self.height_button_continueWithExploit = int((2.0/4)*self.height_cell_frameMainGrid)
        self.width_button_continueWithExploit = int(self.width_cell_frameMainGrid-(2.0/4)*self.height_cell_frameMainGrid)
        self.fontsize_button_continueWithExploit = int(self.fontsize_button_continueWithExploit_SCALE*self.height_cell_frameMainGrid)
        self.button_continueWithExploit = Button(self.frame, text=self.TEXT_BUTTON_CONTINUEWITHEXPLOIT,  bg=self.COLOUR_GENERAL, fg=self.COLOUR_TEXT, font=tkFont.Font(family=self.FONT_GENERAL, size=self.fontsize_button_continueWithExploit),  command=self.proceed_ToExploit)

        #put it on screen
        ##Special Variante 2 hier auskommentieren
        if self.variant_session ==-1:
            self.button_continueWithExploit.place(y=self.yc_button_continueWithExploit, x=self.xc_button_continueWithExploit  , height = self.height_button_continueWithExploit, width = self.width_button_continueWithExploit )        
        
        
        ##Widgets  of the Centre 
        ###general
        self.distance_buttonsToLake = (1.0/4)*self.height_cell_frameMainGrid # abstand zwischen lake-bild und buttons
        self.resizeFactor_baitsToButtons = 0.5
        
        ###LABEL_LAKE
        self.xc_label_lake = int( 2*self.width_cell_frameMainGrid) 
        self.yc_label_lake = int(1*self.height_cell_frameMainGrid)
        self.height_label_lake = int(  3*self.height_cell_frameMainGrid) 
        self.width_label_lake = int(2*self.width_cell_frameMainGrid)         
        self.img_label_lake = view_helpermethods.rescalePicture(self.img_lake, self.width_label_lake, self.height_label_lake) 
        photo_lake = ImageTk.PhotoImage(self.img_label_lake)        
        self.label_lake = Label(self.frame, image=photo_lake, background=self.COLOUR_GENERAL  )
        self.label_lake.image= photo_lake
        #place it on the screen
        self.label_lake.place(x=self.xc_label_lake,y=self.yc_label_lake,width=self.width_label_lake, height = self.height_label_lake)
        
        
        
        ###BUTTON_S
        # Iterieren, platz suchen
        
        #koordinaten der Haken in 18er Koordinatensystem
        #           [0,1,2,3,4,5]
        self.x_lu_button_baits = [x* self.width_cell_frame18Grid for x in  [2,14,4,12,0,16]] #left-upper corner
        self.y_lu_button_baits = [x *self.height_cell_frame18Grid for x in [4,4,4,4,4,4]]
        self.width_button_bait = [x *self.width_cell_frame18Grid for x in [2,2,2,2,2,2]] 
        self.height_button_bait = [x *self.height_cell_frame18Grid for x in [6,6,6,6,6,6]]     
                
        self.img_buttons_bait = [ None for x in  range(0,self.n_baits)]
        for i in range(0,self.n_baits):
            self.img_buttons_bait[i] =  view_helpermethods.rescalePicture(self.imgs_baits[i], 
                                                                          self.width_button_bait[i] *self.resizeFactor_baitsToButtons, 
                                                                          self.height_button_bait[i]*self.resizeFactor_baitsToButtons) 
            
        self.photos_button_bait = [ImageTk.PhotoImage(x) for x in  self.img_buttons_bait ]
        
        self.buttons_bait = [None for x in range(0,self.n_baits)] 
        self.button_id = range(0,self.n_baits)
        
        for i in range(0,self.n_baits):
            
            self.buttons_bait[i] = Button(self.frame, 
                                          image=self.photos_button_bait[i], 
                                          background=self.COLOUR_GENERAL,
                                          command=lambda i=i: self.addAction_bait(i)

                                                                                 
                                          )           
            
            #place it on screen
            self.buttons_bait[i].place(x= self.x_lu_button_baits[i],
                                       y=self.y_lu_button_baits[i], 
                                       width=  self.width_button_bait[i], 
                                       height = self.height_button_bait[i])
            self.buttons_bait[i].image= self.photos_button_bait[i]
        
        #self.buttons_bait[0].command=  lambda : self.addAction_bait(0) 
        
        
        
        ##Widgets of the Bottom        
        ###LABEL feedbackOverallHeading        
        self.xc_label_feedbackOverallHeading = int( self.xc_label_exploreHeading)
        self.yc_label_feedbackOverallHeading = int((4+3.0/8)*self.height_cell_frameMainGrid)
        self.fontsize_label_feedbackOverallHeading = int(  (self.fontsize_label_feedbackOverallHeading_SCALE)*self.height_cell_frameMainGrid)
        self.label_feedbackOverallHeading = Label(self.frame, text=self.TEXT_FEEDBACKOVERALLHEADING,  bg=self.COLOUR_GENERAL, fg=self.COLOUR_TEXT, font = (self.FONT_GENERAL, self.fontsize_label_feedbackOverallHeading))
        self.label_feedbackOverallHeading.place(x=self.xc_label_feedbackOverallHeading,y=self.yc_label_feedbackOverallHeading)           
        
        ### Frame feedbackOverallContainer
        self.xc_frame_feedbackOverallContainer =  int(self.xc_label_feedbackOverallHeading)
        self.yx_frame_feedbackOverallContainer = int((4+2.0/3)*self.height_cell_frameMainGrid) 
        self.width_frame_feedbackOverallContainer =  int(self.width_screen-2*self.xc_frame_feedbackOverallContainer)
        self.height_frame_feedbackOverallContainer = int(1*self.height_cell_frameMainGrid)         
        self.frame_feedbackOverallContainer = Frame(self.frame, background=self.COLOUR_GENERAL)#nur zur hervorhebung!!!             
        self.frame_feedbackOverallContainer.place(x=self.xc_frame_feedbackOverallContainer ,y=self.yx_frame_feedbackOverallContainer, height =self.height_frame_feedbackOverallContainer, width=self.width_frame_feedbackOverallContainer)

        #feedbackOverallUnit
        self.width_label_feedbackOverallUnit= self.width_frame_feedbackOverallContainer*1.0/self.NUNITSPERROW_FRAME_FEEDBACKOVERALLCONTAINER #nicht ge-int-ed, weil dann beerechnung präziser!
        self.height_label_feedbackOverallUnit= self.height_frame_feedbackOverallContainer*1.0/self.NROWS_FRAME_FEEDBACKOVERALLCONTAINER #nicht ge-int-ed, weil dann beerechnung präziser!
        self.n_feedbackOverallContainer_currentUnitInCurrentRow = 0
        self.n_feedbackOverallContainer_currentRow= 0

        
        ###skalierte Bilder für overall feedback     
        rescalefactor = 0.8
        self.img_fishEmpty_feedbackOverall = view_helpermethods.rescalePicture(self.img_fishEmpty, self.width_label_feedbackOverallUnit*rescalefactor, self.height_label_feedbackOverallUnit*rescalefactor)
        self.img_fishFull_feedbackOverall = view_helpermethods.rescalePicture(self.img_fishFull, self.width_label_feedbackOverallUnit*rescalefactor, self.height_label_feedbackOverallUnit)
        self.photo_fishEmpty_feedbackOverall=  ImageTk.PhotoImage(self.img_fishEmpty_feedbackOverall)
        self.photo_fishFull_feedbackOverall=  ImageTk.PhotoImage(self.img_fishFull_feedbackOverall) 
  
        
        self.imgs_bait_FishEmpty_feedbackOverall = [None for x in range(0,self.n_baits)]
        self.imgs_bait_FishFull_feedbackOverall =  [None for x in range(0,self.n_baits)]
        self.photos_bait_FishEmpty_feedbackOverall =  [None for x in range(0,self.n_baits)]
        self.photos_bait_FishFull_feedbackOverall =  [None for x in range(0,self.n_baits)]
                
        for i in range(0,self.n_baits):
            self.imgs_bait_FishEmpty_feedbackOverall[i] = view_helpermethods.rescalePicture(self.imgs_feedback_bait_FishEmpty[i], 
                                                                                            self.width_label_feedbackOverallUnit*rescalefactor, 
                                                                                            self.height_label_feedbackOverallUnit*rescalefactor)
            self.imgs_bait_FishFull_feedbackOverall[i]  =  view_helpermethods.rescalePicture(self.imgs_feedback_bait_FishFull[i], 
                                                                                            self.width_label_feedbackOverallUnit*rescalefactor, 
                                                                                            self.height_label_feedbackOverallUnit*rescalefactor)
            self.photos_bait_FishEmpty_feedbackOverall[i] = ImageTk.PhotoImage(self.imgs_bait_FishEmpty_feedbackOverall[i]) 
            self.photos_bait_FishFull_feedbackOverall[i]  = ImageTk.PhotoImage(self.imgs_bait_FishFull_feedbackOverall[i])         
            
            
         
        
        
        
        
        
        
        
        
        ##Attribute für Trial Feedback (Rahmen um angeklickte Fläche)
        feedbackframe = self.frame #das hier muss noch geändert werden evtl.
        
        self.thickness_frame_feedbackTrial = int((1.0/8*self.height_cell_frameMainGrid))
        
        self.canvases_topBar = [None for x in range(0,self.n_baits)]
        self.canvases_bottomBar = [None for x in range(0,self.n_baits)]
        self.canvases_leftBar = [None for x in range(0,self.n_baits)]
        self.canvases_rightBar = [None for x in range(0,self.n_baits)]
        

        self.x_lu_top = [None for x in range(0,self.n_baits)] 
        self.x_lu_left= [None for x in range(0,self.n_baits)]
        self.x_lu_bottom= [None for x in range(0,self.n_baits)]
        self.x_lu_right= [None for x in range(0,self.n_baits)]
        self.y_lu_top= [None for x in range(0,self.n_baits)]
        self.y_lu_left= [None for x in range(0,self.n_baits)]
        self.y_lu_right= [None for x in range(0,self.n_baits)]
        self.y_lu_bottom = [None for x in range(0,self.n_baits)]
        
        self.width_topBottomBar_Accentuation = [None for x in range(0,self.n_baits)]
        self.height_topBottomBar_Accentuation = [None for x in range(0,self.n_baits)]
        self.width_leftRightBar_Accentuation = [None for x in range(0,self.n_baits)]
        self.height_leftRightBar_Accentuation= [None for x in range(0,self.n_baits)] 
        
        for i in range(0,self.n_baits):
            self.width_topBottomBar_Accentuation[i] = self.width_button_bait[i]+2*self.thickness_frame_feedbackTrial
            self.height_topBottomBar_Accentuation[i] = self.thickness_frame_feedbackTrial               
            self.width_leftRightBar_Accentuation[i] = self.thickness_frame_feedbackTrial
            self.height_leftRightBar_Accentuation[i] = self.height_button_bait[i]+2*self.thickness_frame_feedbackTrial
             
            
            self.x_lu_top[i]    = self.x_lu_button_baits[i]-self.thickness_frame_feedbackTrial
            self.x_lu_left[i]   = self.x_lu_button_baits[i]-self.thickness_frame_feedbackTrial
            self.x_lu_bottom[i] = self.x_lu_button_baits[i]-self.thickness_frame_feedbackTrial
            self.x_lu_right[i]  = self.x_lu_button_baits[i] + self.width_button_bait[i]
            
            
            self.y_lu_top[i]   = self.y_lu_button_baits[i] -self.thickness_frame_feedbackTrial
            self.y_lu_left[i]  = self.y_lu_button_baits[i] -self.thickness_frame_feedbackTrial
            self.y_lu_right[i] = self.y_lu_button_baits[i] -self.thickness_frame_feedbackTrial
            self.y_lu_bottom[i]= self.y_lu_button_baits[i] + self.height_button_bait[i]
            
            
            self.canvases_topBar[i] = Canvas(feedbackframe, bg=self.COLOUR_FRAME_FEEDBACKTRIAL, highlightbackground=self.COLOUR_FRAME_FEEDBACKTRIAL)
            self.canvases_bottomBar[i] = Canvas(feedbackframe, bg=self.COLOUR_FRAME_FEEDBACKTRIAL, highlightbackground=self.COLOUR_FRAME_FEEDBACKTRIAL)
            self.canvases_leftBar[i] = Canvas(feedbackframe, bg=self.COLOUR_FRAME_FEEDBACKTRIAL, highlightbackground=self.COLOUR_FRAME_FEEDBACKTRIAL)
            self.canvases_rightBar[i] = Canvas(feedbackframe, bg=self.COLOUR_FRAME_FEEDBACKTRIAL, highlightbackground=self.COLOUR_FRAME_FEEDBACKTRIAL)
            

        
        #Bilder der Fische skaliert
        
        self.imgs_fishFull_feedbackTrial = [None for x in range(0,self.n_baits)]
        self.imgs_fishEmpty_feedbackTrial  = [None for x in range(0,self.n_baits)]
        
        self.photos_fishFull_feedbackTrial = [None for x in range(0,self.n_baits)]
        self.photos_fishEmpty_feedbackTrial  = [None for x in range(0,self.n_baits)]
        
        
        
        self.xs_img_fish_feedbackTrial= [None for x in range(0,self.n_baits)]     
        self.ys_img_fish_feedbackTrial =  [None for x in range(0,self.n_baits)]
        self.heights_img_fish_feedbackTrial= [None for x in range(0,self.n_baits)]
        self.width_img_fish_feedbackTrial = [None for x in range(0,self.n_baits)]
        

        for i in range(0,self.n_baits):

            self.heights_img_fish_feedbackTrial[i]= int((self.height_button_bait[i])*((1.0-self.resizeFactor_baitsToButtons)/2))
            self.width_img_fish_feedbackTrial[i] = self.width_button_bait[i]
            
            self.imgs_fishFull_feedbackTrial[i] = view_helpermethods.rescalePicture(self.img_fishFull,  
                                                                                    self.width_img_fish_feedbackTrial[i],  
                                                                                    self.heights_img_fish_feedbackTrial[i])
            self.imgs_fishEmpty_feedbackTrial[i]  = view_helpermethods.rescalePicture(self.img_fishEmpty,  
                                                                                    self.width_img_fish_feedbackTrial[i],  
                                                                                    self.heights_img_fish_feedbackTrial[i])
            
            self.heights_img_fish_feedbackTrial[i] = self.imgs_fishFull_feedbackTrial[i].size[1]
            self.width_img_fish_feedbackTrial[i] = self.imgs_fishFull_feedbackTrial[i].size[0]
            
            self.ys_img_fish_feedbackTrial[i]= int(self.y_lu_button_baits[i]+self.height_button_bait[i]-self.heights_img_fish_feedbackTrial[i]*1.1)                                                        
            self.xs_img_fish_feedbackTrial[i]=     self.x_lu_button_baits[i]+(self.width_button_bait[i] -self.width_img_fish_feedbackTrial[i])/2                                
             
          
            self.photos_fishFull_feedbackTrial[i] = ImageTk.PhotoImage(self.imgs_fishFull_feedbackTrial[i])        
            self.photos_fishEmpty_feedbackTrial[i]  = ImageTk.PhotoImage(self.imgs_fishEmpty_feedbackTrial[i]) 
        
        
        #INFO, dass explore beginnt 
        #kann hier weggelassen werden (variante 3)
        
        if self.variant_session!=0:
            explorecontinueframe = Frame(self.master, background=self.COLOUR_GENERAL)
            explorecontinueframe.place(x=0, y=0, width=self.width_screen, height=self.height_screen)
            
            fontsize_intstuctiontext = self.height_screen/self.fontsize_intstuctiontext_SCALE
            label_instructionText = Label(explorecontinueframe, text = self.TEXT_LABEL_ExploreSessionInfo, bg=self.COLOUR_GENERAL, fg=self.COLOUR_TEXT, font=(self.FONT_GENERAL, fontsize_intstuctiontext))
            label_instructionText.place(relx=0.5, rely=0.5, anchor=CENTER)
            
            fontsize_button = self.height_screen/self.fontsize_button_instructiontext_SCALE
            button_explorecontinue = Button(explorecontinueframe, text=self.TEXT_BUTTON_CONTINUETOEXPLORE, command=explorecontinueframe.destroy, font=(self.FONT_GENERAL, fontsize_button),bg=self.COLOUR_GENERAL, fg=self.COLOUR_TEXT)
            button_explorecontinue.place(relx=0.5, rely=0.9, anchor=S)        
        
        
        #TIMER and LOGIC ATTRIBUTES
        self.starttime = time.clock()-self.DURATION_FEEDBACKTRIAL/1000
        self.endtime = 0 
        
        if self.variant_session==0:
            self.proceed_ToExploit()

        
   
   


    def addAction_bait(self, button): #button von 0 bis self.n_baits
                 

        self.endtime = time.clock()
        reactiontime =  self.endtime - self.starttime - self.DURATION_FEEDBACKTRIAL/1000
               

        self.sendInformation(button, reactiontime, 0) #0 für explore
        
        self.add_OverallFeedback(button) #0-self.n_baits
        self.give_TrialFeedback(button) # 0-self.n_baits     
        
        self.n_currentTrial +=1
        
        self.starttime = time.clock()
        
        if (self.n_currentTrial >= self.n_maximumTrial) & (self.variant_session!=-1): #enforce exploit mode
            self.in_explore_mode = False            
            self.frame.after(self.DURATION_FEEDBACKTRIAL, lambda: self.proceed_ToExploit()) 
            
            
       
        
        

         
         
    def add_OverallFeedback(self, button):
        #3 degrees of freedom
        # a) kind of overall feedback: summary or sequence:
        # b) side chosen (only relevant for sequence
        # c) fish caught
        
        
        
        if self.in_explore_mode:
            fish_caught = (self.FISHLIST[self.n_currentTrial % self.n_maximumTrial][button])==1        
        
        if (not self.in_explore_mode): #= exploit modus!
            fish_caught = (self.FISHLIST_EXPLOIT[self.CURRENTNUMBER_OF_EXPLOIT_TRIALS][button])==1
        
        
        if (self.BOOLEAN_POSITIVE_FEEDBACK_ONLY and not(fish_caught)): 
            return
        
        
        colour_background_feedback = self.COLOURLIST_BACKGROUND_OVERALLFEEDBACK[self.i_currentColour_feedbackOverall]
        
        feedback_photo = None
        if self.type_feedbackOverall==0: # nur summary => nur fish
            if fish_caught: #nur voller fisch
                feedback_photo =self.photo_fishFull_feedbackOverall 
            else: #nur leerer Fisch
                feedback_photo =self.photo_fishEmpty_feedbackOverall                
        else: # sequence => fish und bait!
            if fish_caught: #nur voller fisch
                feedback_photo = self.photos_bait_FishFull_feedbackOverall[button]
            else: #nur leerer Fisch
                feedback_photo = self.photos_bait_FishEmpty_feedbackOverall[button]
        
        feedback =  Label(self.frame_feedbackOverallContainer, image=feedback_photo, background=colour_background_feedback)#self.COLOUR_GENERAL)     
        feedback.image = feedback_photo
        
        xc_feedback= int(self.n_feedbackOverallContainer_currentUnitInCurrentRow*self.width_label_feedbackOverallUnit)
        yc_feedback = int(self.n_feedbackOverallContainer_currentRow*self.height_label_feedbackOverallUnit)
        
        feedback.place(x=xc_feedback, y=yc_feedback, width=int(self.width_label_feedbackOverallUnit), height = int(self.height_label_feedbackOverallUnit))         
        
        
        self.n_feedbackOverallContainer_currentUnitInCurrentRow+=1
        
        if(self.n_feedbackOverallContainer_currentUnitInCurrentRow>=self.NUNITSPERROW_FRAME_FEEDBACKOVERALLCONTAINER):
            self.n_feedbackOverallContainer_currentUnitInCurrentRow=0
            self.n_feedbackOverallContainer_currentRow+=1
            if self.n_feedbackOverallContainer_currentRow >= self.NROWS_FRAME_FEEDBACKOVERALLCONTAINER:
                
                self.n_feedbackOverallContainer_currentRow=0
                self.i_currentColour_feedbackOverall +=1
                if self.i_currentColour_feedbackOverall>=len(self.COLOURLIST_BACKGROUND_OVERALLFEEDBACK):
                    self.i_currentColour_feedbackOverall = 0
    
    def give_TrialFeedback(self, button):        
        
        feedback = Frame(self.frame, bg="") # mit "" wirds transparent!        
        feedback.place(x=0,y=0,width=self.width_screen, height= self.yc_label_feedbackOverallHeading+self.fontsize_label_exploreHeading)
        
        #Rahmen malen        
        self.drawFrame_FeedbackTrial(feedback, button)
        #Fische einzeichnen
        self.drawFish_FeedbackTrial(feedback, button)
        if self.type_feedbackTrial==1: #vollständig
            other_buttons = range(0,self.n_baits)
            other_buttons.remove(button)
            for i in other_buttons:
                self.drawFish_FeedbackTrial(feedback, i)        
        
        
        cover_top = Frame(self.frame, bg=self.COLOUR_GENERAL) # Schutz vor klick
        cover_top.place(x=int(5*self.width_cell_frameMainGrid),y=0, width=self.width_cell_frameMainGrid, height=self.height_cell_frameMainGrid-self.thickness_frame_feedbackTrial)
        
        feedback.after(self.DURATION_FEEDBACKTRIAL, lambda:feedback.destroy())
        cover_top.after(self.DURATION_FEEDBACKTRIAL, lambda:cover_top.destroy())
                  
    def  drawFrame_FeedbackTrial(self, feedback, button):
        top = Canvas(feedback, bg=self.COLOUR_FRAME_FEEDBACKTRIAL, highlightbackground=self.COLOUR_FRAME_FEEDBACKTRIAL)
        left = Canvas(feedback, bg=self.COLOUR_FRAME_FEEDBACKTRIAL, highlightbackground=self.COLOUR_FRAME_FEEDBACKTRIAL)
        bottom = Canvas(feedback, bg=self.COLOUR_FRAME_FEEDBACKTRIAL, highlightbackground=self.COLOUR_FRAME_FEEDBACKTRIAL)
        right = Canvas(feedback, bg=self.COLOUR_FRAME_FEEDBACKTRIAL, highlightbackground=self.COLOUR_FRAME_FEEDBACKTRIAL) 
        #draw
        top.place(x= self.x_lu_top[button],
                  y= self.y_lu_top[button],
                  width=  self.width_topBottomBar_Accentuation[button],                  
                  height=self.height_topBottomBar_Accentuation[button])
        
        left.place(x= self.x_lu_left[button],
                      y= self.y_lu_left[button],
                      width=  self.width_leftRightBar_Accentuation[button],                  
                      height=self.height_leftRightBar_Accentuation[button])           
        
        bottom.place(x= self.x_lu_bottom[button],
                      y= self.y_lu_bottom[button],
                      width=  self.width_topBottomBar_Accentuation[button],                  
                      height=self.height_topBottomBar_Accentuation[button])
            
        right.place(x= self.x_lu_right[button],
                      y= self.y_lu_right[button],
                      width=  self.width_leftRightBar_Accentuation[button],                  
                      height=self.height_leftRightBar_Accentuation[button])         
      
    
    
    
    def drawFish_FeedbackTrial(self, feedback, button):
        if self.in_explore_mode:
            fish_underneath = (self.FISHLIST[self.n_currentTrial % self.n_maximumTrial][button])==1
        elif (not self.in_explore_mode): 
            fish_underneath = (self.FISHLIST_EXPLOIT[self.CURRENTNUMBER_OF_EXPLOIT_TRIALS][button])==1
        
        fish_label = None
        
        if fish_underneath:
            fish_label = Label(feedback, bg=self.COLOUR_GENERAL, image=self.photos_fishFull_feedbackTrial[button])            
            fish_label.image = self.photos_fishFull_feedbackTrial[button]
            
        else:
            fish_label = Label(feedback, bg=self.COLOUR_GENERAL, image=self.photos_fishEmpty_feedbackTrial[button])            
            fish_label.image = self.photos_fishEmpty_feedbackTrial[button]           
            
        fish_label.place(x=self.xs_img_fish_feedbackTrial[button],
                         y=self.ys_img_fish_feedbackTrial[button]) 
        
        
       
                    
                        
        
    def proceed_ToExploit(self):
        
        #Info über Exploit! (nur beim ersten mal)
        if (self.CURRENTNUMBER_OF_EXPLOIT_TRIALS==0):
            self.build_ExploitSessionInfoView()
            
            # "Lösche alles was auf dem Overallcontainer frame steht"      
            for widget in self.frame_feedbackOverallContainer.winfo_children():
                widget.destroy()
            # "initialisiere die positionen wieder auf 0 0 "
            self.n_feedbackOverallContainer_currentUnitInCurrentRow=0
            self.n_feedbackOverallContainer_currentRow = 0
            # Hintergrundfarbe wieder schwarz
            self.i_currentColour_feedbackOverall = 0
             
        
        #1. alles überflüssige zerstören
        #self.label_feedbackOverallHeading.destroy()
        self.button_continueWithExploit.destroy()
        #self.frame_feedbackOverallContainer.destroy()
        

        
        
        
        #reconfigure elements
        self.label_mainHeading.configure(text=self.TEXT_EXPLOITHEADING)   
        
        # Modus verändern:
        self.in_explore_mode = False     
        
        #buttonpressfunctions neue fuktionen    
        for i in range(0,self.n_baits):
            self.buttons_bait[i].configure(command=lambda i=i:self.buttonPress_exploit(i))     

        self.starttime =time.clock()
    
    def build_ExploitSessionInfoView(self):     
        starttime = time.time()
        exploitcontinueframe = Frame(self.master, background=self.COLOUR_GENERAL)
        exploitcontinueframe.place(x=0, y=0, width=self.width_screen, height=self.height_screen)
        
        fontsize_instruction_ExploitSessionInfo_text = self.height_screen/self.fontsize_instruction_ExploitSessionInfo_text_SCALE # 20
        label_instructionText = Label(exploitcontinueframe, text = self.TEXT_LABEL_ExploitSessionInfo, bg=self.COLOUR_GENERAL, fg=self.COLOUR_TEXT, font=(self.FONT_GENERAL, fontsize_instruction_ExploitSessionInfo_text))
        label_instructionText.place(relx=0.5, rely=0.1, anchor=N)
        
              

        label_quest1 = Label(exploitcontinueframe, text = self.TEXT_LABEL_ExploitSessionInfo_Quest_NFishes, bg=self.COLOUR_GENERAL, fg=self.COLOUR_TEXT, font=(self.FONT_GENERAL, fontsize_instruction_ExploitSessionInfo_text))
        label_quest1.place(relx=0.5, rely=0.2, anchor=N)        
        scale_nfishes = Scale(exploitcontinueframe, from_=0,to=self.SCALE_ESTIMATION_UPPERLIMIT,
                          fg = "yellow",bg="black", 
                          orient=HORIZONTAL,
                          font=tkFont.Font(family=self.FONT_GENERAL, size=self.height_screen/24), 
                          width=(self.height_screen/24)) 
        scale_nfishes.set(0)
        scale_nfishes.place(relx=0.5, rely=0.3,width=(self.width_screen/3), anchor=N) 
        
     
                 
        
        label_quest2 = Label(exploitcontinueframe, text = self.TEXT_LABEL_ExploitSessionInfo_Quest_Confidence, bg=self.COLOUR_GENERAL, fg=self.COLOUR_TEXT, font=(self.FONT_GENERAL, fontsize_instruction_ExploitSessionInfo_text))
        label_quest2.place(relx=0.5, rely=0.5, anchor=N)       
        confidence_var = IntVar()
        confidence_var.set(0)
        likertskala = self.likertskala_conficence

        confidenceframe = Frame(exploitcontinueframe, background=self.COLOUR_GENERAL)
        confidenceframe.place(relx=0.5, rely=0.55, anchor = N, width=self.width_screen/3, height=self.height_screen/6)
        for (verbal, number) in likertskala:
            b = Radiobutton(confidenceframe, 
                            text=str(number),
                            font=tkFont.Font(family=self.FONT_GENERAL, size=self.height_screen/24), 
                            variable= confidence_var,
                            value=number,
                            indicatoron = 0,                             
                            fg = "green",bg="black")
            b.place(relx=(number-1)/7.0,rely=0,width=(self.width_screen/21),height= self.height_screen/12)
            l = Label(confidenceframe, text =verbal,
                      bg=self.COLOUR_GENERAL, 
                      fg=self.COLOUR_TEXT, 
                      font=(self.FONT_GENERAL, 10) )
            l.place(relx=(number-1)/7.0,rely=0.5,width=(self.width_screen/21),height= self.height_screen/12)
        
        #button 
        def button_exploitcontinue_press():
            self.estimated_caught_nfish = scale_nfishes.get()            
            self.estimated_confidence = confidence_var.get()
            if  self.estimated_confidence==0: # noch keine Auswahl
                return            
            #Infos übermitteln
            endtime = time.time()
            time_needed = endtime-starttime
            self.view.controler.view_confidenceData(self.estimated_caught_nfish,self.estimated_confidence,time_needed)
            exploitcontinueframe.destroy()   
        
        fontsize_button_ExploitSessionInfo = self.height_screen/self.fontsize_button_ExploitSessionInfo_SCALE #30
        button_exploitcontinue = Button(exploitcontinueframe, 
                                        text=self.TEXT_BUTTON_CONTINUETOEXPLOIT, 
                                        command= button_exploitcontinue_press, 
                                        font=(self.FONT_GENERAL, fontsize_button_ExploitSessionInfo),
                                        bg=self.COLOUR_GENERAL, fg=self.COLOUR_TEXT)
        button_exploitcontinue.place(relx=0.5, rely=0.9, anchor=N)
        

        
        ##Read out info (check whether valid
        ##Send it to model (write it to file)
        ##close view
        
        
        
    def buttonPress_exploit(self, button):
        
        self.endtime = time.clock()
        reactiontime =  self.endtime - self.starttime   


        self.give_TrialFeedback(button)  
        
        #print "button gepresst EXPLOIT"  
        self.sendInformation(button, reactiontime, 1)
        # self.frame.after(self.DURATION_FEEDBACKTRIAL, lambda: self.frame.destroy())       
        
        
        self.add_OverallFeedback(button)
        
        
        if (self.CURRENTNUMBER_OF_EXPLOIT_TRIALS <   self.NUMBER_OF_EXPLOIT_TRIALS-1):
            self.frame.after(self.DURATION_FEEDBACKTRIAL, lambda: self.proceed_ToExploit()) 
            
            self.CURRENTNUMBER_OF_EXPLOIT_TRIALS +=1
        else:   
            self.frame.after(self.DURATION_FEEDBACKTRIAL, lambda: self.build_afterSessionView()) 
        
                
        
            

    def sendInformation(self, button, reactiontime, explore_exploit): # explore_exploit: 0 für explore, 1 für exploit
        self.view.controler.view_trialData(self.n_currentTrial, button, reactiontime, explore_exploit)
        
    def defineAttributes_initialiseFrame(self):
        self.frame=Frame(self.master, background=self.COLOUR_GENERAL)
        self.frame.place(x=0, y=0, width=self.width_screen, height=self.height_screen)
        
    
    def build_afterSessionView(self):
        #restore master
        self.defineAttributes_initialiseFrame()        
        #label
        
        label_text_aftersession = Label(self.frame, text = self.TEXT_LABEL_AFTERSESSION,
                                         bg=self.COLOUR_GENERAL, fg=self.COLOUR_TEXT, 
                                         font=(self.FONT_GENERAL, self.fontsize_button_continueWithExploit*2))
        label_text_aftersession.place(relx=0.5, rely=0.5, anchor=CENTER)  
        
        #button anlegen
        buttonContinue =  Button(self.frame, text = self.TEXT_BUTTON_AFTERSESSION, 
                                 command=lambda: self.addAction_buttonContinue(), 
                                 background=self.COLOUR_GENERAL, 
                                 fg=self.COLOUR_TEXT, 
                                 font=tkFont.Font(family=self.FONT_GENERAL, size=self.fontsize_button_continueWithExploit)) 

        buttonContinue.place(relx=0.5, rely =0.9,anchor =CENTER, )

        
    def addAction_buttonContinue(self):
        self.view.controler.view_endSession()        
