'''
Created on 03.07.2017

@author: Martin
'''

from Tkinter import  *
import view_experimentSession
import view_instruction
import view_vpndata
import view_end
import view_end_tableWithResults


class View(object):
    '''
    classdocs
    '''


    def __init__(self, controler, root):
        '''
        Constructor
        '''
        
        self.controler = controler # controler!
        
        self.root = root
        
        self.vpndataView=None
        self.instuctionView= None
        self.experimentalView=None
        self.endView=None
        
        
        
        
    
    
    def show_vpndataView(self):
        self.refresh_root()        
        self.vpndataView= view_vpndata.VpnDataView(self,self.root)
        
    
    def show_instructionView(self):
        self.refresh_root()
        self.instuctionView= view_instruction.InstructionView(self,self.root)
        
    def show_experimentalView(self,
                                 variant_session,
                                 type_feedbackTrial, 
                                 type_feedbackOverall, 
                                 fishList, 
                                 fishList_Exploit, 
                                 path_img_lake,
                                 n_baits, 
                                 paths_img_baits, #list of paths to baits
                                 paths_img_feedback_fishfull,
                                 paths_img_feedback_fishempty):
        
        
        self.refresh_root()
        
        
        
        self.experimentalView = view_experimentSession.ExperimentalView(self, 
                                                                        self.root, 
                                                                         variant_session,
                                                                         type_feedbackTrial, 
                                                                         type_feedbackOverall, 
                                                                         fishList, 
                                                                         fishList_Exploit, 
                                                                         path_img_lake,
                                                                         n_baits, 
                                                                         paths_img_baits, #list of paths to baits
                                                                         paths_img_feedback_fishfull,
                                                                         paths_img_feedback_fishempty)
        
    def show_endView(self):
        self.refresh_root()
        self.endView = view_end.EndView(self,self.root)
        
    def show_EndTableView(self, n_lakes,list_n_fish_caught,list_n_trials):       
        self.refresh_root()       
        self.endTableView = view_end_tableWithResults.EndTable_View(view=self, master=self.root, n_lakes=n_lakes,list_n_fish_caught=list_n_fish_caught,list_n_trials=list_n_trials )
        
    def close_view(self):
        self.root.quit()
        
    def refresh_root(self):
        for widget in self.root.winfo_children():
            widget.destroy()
    