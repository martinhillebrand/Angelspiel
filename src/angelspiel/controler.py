# -*- coding: utf-8 -*
'''
Created on 03.07.2017

@author: Martin
'''

import view
import model
from Tkinter import  *

class Controler(object):
    
    def __init__(self):        
        self.root = Tk()
        self.model = model.Model(self)        
        self.view = view.View(self, self.root)
        self.controler_showVpndataView()
    
    #schema QUELLE_GENAUEQUELLE_BOTSCHAFT
    def controler_showVpndataView(self):
        self.view.show_vpndataView()
    
    def view_vpnData_finished(self, age, sex):
        self.model.setAgeSex(age,sex)
        self.view.show_instructionView()        
        
    
    def view_instructionAccepted(self):
        self.model.instructionAccepted()
        
        
    def model_startSession(self,
                             variant_session,
                             type_feedbackTrial, 
                             type_feedbackOverall, 
                             fishList, 
                             fishList_Exploit, 
                             path_img_lake,
                             n_baits, 
                             paths_img_baits, #list of paths to baits
                             paths_img_feedback_fishfull,
                             paths_img_feedback_fishempty):
        
        
        self.view.show_experimentalView(variant_session,
                                         type_feedbackTrial, 
                                         type_feedbackOverall, 
                                         fishList, 
                                         fishList_Exploit, 
                                         path_img_lake,
                                         n_baits, 
                                         paths_img_baits, #list of paths to baits
                                         paths_img_feedback_fishfull,
                                         paths_img_feedback_fishempty)
        
        
    def view_trialData(self, n_currentTrial, button, reactiontime, explore_exploit): # is called by experimentsessionview
        #n_currentTrial (nummer, beginnend bei 0 bis maximum, button 0 link, 1 rechts, reactiontime in seconds, explore_exploit 0 explore, 1 exploit
        self.model.add_trialData(n_currentTrial, button, reactiontime, explore_exploit)
    
    def view_confidenceData(self, estimated_caught_nfish, estimated_confidence,time_needed):
        self.model.add_confidenceData(estimated_caught_nfish, estimated_confidence,time_needed)    
    
    def view_endSession(self):
        self.model.endSession() # im model wird geschaut ob noch eine session kommt, model ruft dann model_startSession auf
        #falls nicht -> speichere dokument, schließe es und rufe model_showEndview auf
   
    
    def model_showEndview(self):
        self.view.show_endView()
        
        
    def view_showScore(self):
        self.model.showScore() # im Model werden alle Scores zusammengezählt und von dort aus die view ausgerufen
        
    def model_showScore(self,n_lakes,list_n_fish_caught,list_n_trials ):
        self.view.show_EndTableView(n_lakes=n_lakes,list_n_fish_caught=list_n_fish_caught,list_n_trials=list_n_trials)    

        
    def view_closeAll(self):
        self.quit()
 
    def run(self):
        self.root.mainloop()
        
    def quit(self):
        self.root.quit()    
    

