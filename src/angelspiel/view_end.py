# -*- coding: utf-8 -*
'''
Created on 03.07.2017

@author: Martin
'''

from Tkinter import  *
from PIL import Image, ImageTk
import time 
import tkFont
import view_helpermethods

class EndView(object):
    
    def __init__(self,view, master):
        """ zu verändernder Text hier:"""
        """
        ++++++++++++++++++++++++++++++++++++++
        """
        
        self.TEXT_LABEL_INSTRUCTIONTEXT = "Vielen Dank für die Teilnahme\nBitte informiere den Versuchsleiter."
        self.TEXT_BUTTON_START="Gesamtscore betrachten"
        self.FONT_GENERAL="Arial" 
        self.COLOUR_TEXT = "white"       
        self.COLOUR_GENERAL="black"
        self.fontsize_intstuctiontext_SCALE = 20
        self.fontsize_button_SCALE = 30
        """
        ++++++++++++++++++++++++++++++++++++++
        """
        self.view = view
        
        self.master = master
        self.master.attributes('-fullscreen', True)
        self.master.configure(background=self.COLOUR_GENERAL)
        
        self.width_screen = self.master.winfo_screenwidth()
        self.height_screen = self.master.winfo_screenheight()
        
        self.frame=Frame(self.master, background=self.COLOUR_GENERAL)
        self.frame.place(x=0, y=0, width=self.width_screen, height=self.height_screen)
        
        fontsize_intstuctiontext = self.height_screen/self.fontsize_intstuctiontext_SCALE
        self.label_instructionText = Label(self.frame, text = self.TEXT_LABEL_INSTRUCTIONTEXT, bg=self.COLOUR_GENERAL, fg=self.COLOUR_TEXT, font=(self.FONT_GENERAL, fontsize_intstuctiontext))
        self.label_instructionText.place(relx=0.5, rely=0.5, anchor=CENTER)
        
        fontsize_button = self.height_screen/self.fontsize_button_SCALE
        self.button_start = Button(self.master, text=self.TEXT_BUTTON_START, command=self.end, font=(self.FONT_GENERAL, fontsize_button),bg=self.COLOUR_GENERAL, fg=self.COLOUR_TEXT,)
        self.button_start.place(relx=0.5, rely=0.9, anchor=S)
    
    def end(self):
        #self.view.controler.view_closeAll()
        self.view.controler.view_showScore()
        
        
